package main

import (
	"auth-service/api"
	"auth-service/api/handlers"
	"fmt"
)

func main() {
	handler := handlers.NewHandler()

	roter := api.NewRouter(handler)
	fmt.Println("Server is running on port ", ":8000")
	if err := roter.Run(":8000"); err != nil {
		panic(err)
	}
}
