package handlers

import (
	"auth-service/api/token"
	"auth-service/models"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/swag"
)

type HTTPHandler struct{}

func NewHandler() *HTTPHandler {
	return &HTTPHandler{}
}

// Login godoc
// @Summary Login a user
// @Description Authenticate user with email and password
// @Tags auth
// @Accept json
// @Produce json
// @Param credentials body models.LoginReq true "User login credentials"
// @Success 200 {object} token.Tokens "JWT tokens"
// @Failure 400 {object} string "Invalid request payload"
// @Failure 401 {object} string "Invalid email or password"
// @Router /login [post]
func (h *HTTPHandler) Login(c *gin.Context) {
	req := models.LoginReq{}

	if err := c.BindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"Invalid request payload": err.Error()})
		return
	}

	if req.Username != "admin" || req.Password != "password" {
		c.JSON(http.StatusUnauthorized, gin.H{"erorr": "Invalid email or password. Please enter for Username (admin) and for the password (password)"})
		return
	}

	tokens := token.GenerateJWTToken(req.Username, req.Password)

	c.JSON(http.StatusOK, tokens)
}
