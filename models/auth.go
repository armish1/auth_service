package models

type LoginReq struct {
	Username string `json:"username"` // Admin's username
	Password string `json:"password"` // Admin's password
}
